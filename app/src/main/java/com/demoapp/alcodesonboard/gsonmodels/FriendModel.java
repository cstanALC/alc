package com.demoapp.alcodesonboard.gsonmodels;

import com.demoapp.alcodesonboard.database.entities.MyFriends;

import java.util.ArrayList;

public class FriendModel {
    public String id;
    public String email;
    public String first_name;
    public String last_name;
    public String avatar;
    public String page;
    public ArrayList<MyFriends> data;
}
