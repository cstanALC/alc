package com.demoapp.alcodesonboard.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.demoapp.alcodesonboard.activities.MyFriendsActivity;
import com.demoapp.alcodesonboard.activities.ShowActivity;
import com.demoapp.alcodesonboard.database.entities.MyFriends;
import com.demoapp.alcodesonboard.fragments.MyFriendsFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyFriendsAdapter extends RecyclerView.Adapter<MyFriendsAdapter.ViewHolder> {
    private List<DataHolder> mData = new ArrayList<>();


    private Callbacks mCallbacks;



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friends, parent, false));

    }
    public void setData(List<MyFriendsAdapter.DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);


    }



    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public Long id;
        public String first_name;
        public String last_name;
        public String email;
        public String avatar;

    }


    public static class ViewHolder extends RecyclerView.ViewHolder  {


        @BindView(R.id.linearlayout_root)
        public RelativeLayout root;
        @BindView(R.id.textview_friend_name)
        public TextView mtvName;

        @BindView(R.id.textview_friend_email)
        protected TextView mTV_email;

        @BindView(R.id.icon_friend)
        protected ImageView ivUser;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
//
            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {

            if (data != null) {

                if (callbacks != null) {
mtvName.setText(data.first_name+" "+data.last_name);
mTV_email.setText(data.email);
                    Glide.with(itemView.getContext()).load(data.avatar).into(ivUser);

/*

                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }); */
                    /*
                    Glide.with(context).load(myFriends.getAvatar()).into(holder.ivUser);
                    */

                    ivUser.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent=new Intent(itemView.getContext(), ShowActivity.class);
                            intent.putExtra("url",data.avatar.toString());
                            itemView.getContext().startActivity(intent);
                        }
                    });



                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            /*
                            Intent intent = new Intent(itemView.getContext(), MyFriendDetailActivity.class);
                            intent.putExtra("id",data.id.toString());

                            itemView.getContext().startActivity(intent);*/


                                callbacks.onListItemClicked(data);


                        }
                    });

                }
            }
        }



    }

    public interface Callbacks {

        void onListItemClicked(DataHolder data);

    }
}
