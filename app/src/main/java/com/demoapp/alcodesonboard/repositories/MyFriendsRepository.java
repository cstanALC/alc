package com.demoapp.alcodesonboard.repositories;

import android.app.ProgressDialog;
import android.content.Context;
import android.provider.ContactsContract;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.adapters.MyNotesAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriends;
import com.demoapp.alcodesonboard.database.entities.MyFriendsDao;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.database.entities.MyNoteDao;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyFriendsRepository {
    private static MyFriendsRepository mInstance;

    private MutableLiveData<MyFriends> mMyFriendLiveData = new MutableLiveData<>();

    private MutableLiveData<List<MyFriendsAdapter.DataHolder>> mMyFriendsAdapterListLiveData = new MutableLiveData<>();
    public static MyFriendsRepository getInstance() {
        if (mInstance == null) {
            synchronized (MyFriendsRepository.class) {
                mInstance = new MyFriendsRepository();
            }
        }

        return mInstance;
    }
    public MyFriendsRepository() {
    }
    public LiveData<List<MyFriendsAdapter.DataHolder>> getMyFriendsAdapterListLiveData() {
        return mMyFriendsAdapterListLiveData;
    }


    public void loadMyFriendsAdapterList(Context context) {









        List<MyFriendsAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyFriends> records = DatabaseHelper.getInstance(context)
                .getMyFriendsDao()
                .loadAll();


        if (records != null) {
            for (MyFriends myFriends : records) {
                MyFriendsAdapter.DataHolder dataHolder = new MyFriendsAdapter.DataHolder();
                dataHolder.id = myFriends.getId();
                dataHolder.first_name = myFriends.getFirstName();
                dataHolder.last_name=myFriends.getLastName();
                dataHolder.email=myFriends.getEmail();
                dataHolder.avatar=myFriends.getAvatar();
                dataHolders.add(dataHolder);
            }
        }

        mMyFriendsAdapterListLiveData.setValue(dataHolders);
    }



    public LiveData<MyFriends> getMyFriendsLiveData() {
        return mMyFriendLiveData;
    }

    public void getfriendlist(Context context){

        String url = BuildConfig.BASE_API_URL + "users?page=1";
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray Jarry=response.getJSONArray("data");
                    for(int i=0;i<Jarry.length();i++){
                        MyFriends friend= new MyFriends();

                        JSONObject data=Jarry.getJSONObject(i);
                        friend.setId(Long.valueOf(data.getString("id")));
                        friend.setFirstName(data.getString("first_name"));
                        friend.setLastName(data.getString("last_name"));
                        friend.setEmail(data.getString("email"));
                        friend.setAvatar(data.getString("avatar"));
                        DatabaseHelper.getInstance(context)
                                .getMyFriendsDao()
                                .insertOrReplace(friend);

                        // Done adding record, now re-load list.
                        loadMyFriendsAdapterList(context);
                    }


                }


                catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MaterialDialog md=   new MaterialDialog.Builder(context)
                        .title("Error")
                        .content("No Internet Connection").show();
            }
        });
        {
            NetworkHelper.getRequestQueueInstance(context).add(stringRequest);

        }
    }
    public void loadMyFriendById(Context context, Long id) {
        MyFriends myFriends = DatabaseHelper.getInstance(context)
                .getMyFriendsDao()
                .load(id);

        mMyFriendLiveData.setValue(myFriends);
    }











}
