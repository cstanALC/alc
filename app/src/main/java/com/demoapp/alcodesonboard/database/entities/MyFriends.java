package com.demoapp.alcodesonboard.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

@Entity
public class MyFriends {
    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String FirstName;

    @NotNull
    private String LastName;

    @NotNull
    private String Email;
private  String avatar;


    @Generated(hash = 2016274451)
    public MyFriends(Long id, @NotNull String FirstName, @NotNull String LastName, @NotNull String Email, String avatar) {
        this.id = id;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Email = Email;
        this.avatar = avatar;
    }

    @Generated(hash = 1574580815)
    public MyFriends() {
    }



    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return this.LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getEmail() {
        return this.Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
