package com.demoapp.alcodesonboard.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.demoapp.alcodesonboard.database.entities.DaoMaster;

public class DbOpenHelper extends DaoMaster.OpenHelper {

    public DbOpenHelper(Context context, String name) {
        super(context, name);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
    }
}
