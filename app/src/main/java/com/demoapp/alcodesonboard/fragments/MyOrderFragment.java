package com.demoapp.alcodesonboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyOrder;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyOrderFragment extends Fragment
{
    public static final String TAG = MyOrderFragment.class.getSimpleName();
    private final int REQUEST_CODE_MY_NOTE_DETAIL = 300;
    private Unbinder mUnbinder;

    public MyOrderFragment()
    {
    }
    public static MyOrderFragment newInstance() {
        return new MyOrderFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_notes, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

}
