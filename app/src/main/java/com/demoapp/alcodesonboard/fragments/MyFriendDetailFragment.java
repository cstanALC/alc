package com.demoapp.alcodesonboard.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.demoapp.alcodesonboard.activities.MyFriendsActivity;
import com.demoapp.alcodesonboard.activities.ShowActivity;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriends;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModelFactory;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MyFriendDetailFragment extends Fragment {

    public static final String TAG = MyFriendDetailFragment.class.getSimpleName();
public  MyFriendDetailFragment(){}
    private ArrayList Friendlist;
    private Unbinder mUnbinder;
    @BindView(R.id.edittext_FirstName)
    protected TextInputEditText mfname;
    @BindView(R.id.edittext_LastName)
    protected TextInputEditText lname;
    @BindView(R.id.profile_image)
    protected ImageView proI;
    @BindView(R.id.edittext_email)
    protected TextInputEditText memail;

    @BindView(R.id.sendEmail) //commit test
    protected Button  mSendEmail;
    Long myfriendid=0L;
String imgpath;
    private MyFriendsViewModel mViewModel;
    public static MyFriendDetailFragment newInstance() {
        return new MyFriendDetailFragment();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        Friendlist=new ArrayList<>();
getfriendlist();
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friend_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        myfriendid=Long.parseLong(getActivity().getIntent().getExtras().getString("id"));



    }
    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }
    @OnClick(R.id.profile_image)
    public void goFullphoto(){
        Intent intent=new Intent(getContext(), ShowActivity.class);
        intent.putExtra("url", imgpath);
       getContext().startActivity(intent);


    }

    @OnClick(R.id.sendEmail)
            public void toSendEmail(){
        String email=friend.getEmail();

        Intent intent=new Intent(Intent.ACTION_SEND);
        String[] recipients={memail.getText().toString()};
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);

        intent.setType("text/html");
        intent.setPackage("com.google.android.gm");
        startActivity(Intent.createChooser(intent, "Send mail"));
    }

    MyFriends friend= new MyFriends();
    public void getfriendlist(){
        final ProgressDialog pd = ProgressDialog.show(getContext(), null, "Loading");

        String id=getActivity().getIntent().getExtras().getString("id");

        String url = BuildConfig.BASE_API_URL + "users/"+id;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                try {
                    JSONObject data=new JSONObject(response);

                    JSONObject Jarry=data.getJSONObject("data");



                    mfname.setText(Jarry.getString("first_name"));
                    lname.setText(Jarry.getString("last_name"));
                    memail.setText(Jarry.getString("email"));
                    Glide.with(getContext()).load(Jarry.getString("avatar")).into(proI);
imgpath=Jarry.getString("avatar");


                }

                catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                MaterialDialog md=   new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content("No Internet Connection").show();
            }
        });
        {
            NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);

        }
    }






}
