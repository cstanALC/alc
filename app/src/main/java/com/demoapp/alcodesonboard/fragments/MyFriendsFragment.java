package com.demoapp.alcodesonboard.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.widget.Adapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.LoginActivity;
import com.demoapp.alcodesonboard.activities.MainActivity;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.demoapp.alcodesonboard.activities.MyFriendsActivity;
import com.demoapp.alcodesonboard.activities.MyNoteDetailActivity;
import com.demoapp.alcodesonboard.activities.ShowActivity;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.adapters.MyNotesAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriends;
import com.demoapp.alcodesonboard.gsonmodels.FriendModel;
import com.demoapp.alcodesonboard.gsonmodels.LoginModel;
import com.demoapp.alcodesonboard.repositories.MyFriendsRepository;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModelFactory;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModelFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import timber.log.Timber;
import android.os.Handler;

public class MyFriendsFragment extends Fragment implements MyFriendsAdapter.Callbacks {
    public static final String TAG = MyFriendsFragment.class.getSimpleName();
    public MyFriendsFragment() {
    }
    @BindView(R.id.recyclerview)
    protected RecyclerView mRecyclerView;
    private ArrayList<MyFriends> Friendlist;
    private Unbinder mUnbinder;
    private final int REQUEST_CODE_MY_Friend_list = 300;


    private MyFriendsAdapter mAdapter;

    private MyFriendsViewModel mViewModel;

    public static MyFriendsFragment newInstance() {
        return new MyFriendsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
Friendlist=new ArrayList<>();

    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friendlist, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }







    private void initView() {
        //show friend list
        mAdapter = new MyFriendsAdapter();
        mAdapter.setCallbacks(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }
    private void initViewModel() {
        final ProgressDialog pd = ProgressDialog.show(getContext(), null, "Loading");

//        mViewModel = ViewModelProviders.of(this).get(MyNotesViewModel.class);
        mViewModel = new ViewModelProvider(this, new MyFriendsViewModelFactory(getActivity().getApplication())).get(MyFriendsViewModel.class);
        mViewModel.getMyFriendsAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<MyFriendsAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<MyFriendsAdapter.DataHolder> dataHolders) {

               // mAdapter.setData(dataHolders);
             //   mAdapter.notifyDataSetChanged();

                if(dataHolders==null){
mRecyclerView.setVisibility(View.INVISIBLE);

                }
                 mAdapter.setData(dataHolders);
                  mAdapter.notifyDataSetChanged();
            }
        });

        // Load data into adapter.
        mViewModel.getFriendlist();
        pd.dismiss();

    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();



        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_MY_Friend_list && resultCode == MyNoteDetailActivity.RESULT_CONTENT_MODIFIED) {
            // Child report content is changed, re-load list.
            mViewModel.loadFriendsAdapterList();
        }
    }




    @Override
    public void onListItemClicked(MyFriendsAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), MyFriendDetailActivity.class);
        intent.putExtra("id", data.id.toString());

        startActivity(intent);
    }


}
