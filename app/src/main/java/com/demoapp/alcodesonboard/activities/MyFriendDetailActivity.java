package com.demoapp.alcodesonboard.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.MainFragment;
import com.demoapp.alcodesonboard.fragments.MyFriendDetailFragment;
import com.demoapp.alcodesonboard.fragments.MyFriendsFragment;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;

import butterknife.ButterKnife;

public class MyFriendDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_friend_detail);


        ButterKnife.bind(this);


            FragmentManager fragmentManager = getSupportFragmentManager();

            if (fragmentManager.findFragmentByTag(MyFriendDetailFragment.TAG) == null) {

                fragmentManager.beginTransaction()
                        .replace(R.id.framelayout_fragment_holder, MyFriendDetailFragment.newInstance(), MyFriendDetailFragment.TAG)
                        .commit();
            }

    }
}
