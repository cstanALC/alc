package com.demoapp.alcodesonboard.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.MyNotesFragment;
import com.demoapp.alcodesonboard.fragments.MyOrderFragment;

import butterknife.ButterKnife;

public class MyOrder extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);
        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(MyOrderFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holderOrder, MyOrderFragment.newInstance(), MyOrderFragment.TAG)
                    .commit();
        }
    }
}
