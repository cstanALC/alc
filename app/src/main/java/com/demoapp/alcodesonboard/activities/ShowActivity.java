package com.demoapp.alcodesonboard.activities;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.MainFragment;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowActivity extends AppCompatActivity {

    @BindView(R.id.fullphoto)
    protected ImageView mFullPhoto;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.showimage);
        ButterKnife.bind(this);

        Intent intent=getIntent();
        String URL = intent.getStringExtra("url");
        Glide.with(this).load(URL).into(mFullPhoto);


        }

}
