package com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.adapters.MyNotesAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriends;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.repositories.MyFriendsRepository;
import com.demoapp.alcodesonboard.repositories.MyNotesRepository;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;

import java.util.List;

public class MyFriendsViewModel extends AndroidViewModel{



        private MyFriendsRepository mMyFriendsRepository;

    public MyFriendsViewModel(@NonNull Application application) {
        super(application);

        mMyFriendsRepository = MyFriendsRepository.getInstance();
    }

    public LiveData<MyFriends> getMyFriendsLiveData() {
        return mMyFriendsRepository.getMyFriendsLiveData();
    }
   public void loadMyFriendById(Long id){mMyFriendsRepository.loadMyFriendById(getApplication(),id);}



        public LiveData<List<MyFriendsAdapter.DataHolder>> getMyFriendsAdapterListLiveData() {
        return mMyFriendsRepository.getMyFriendsAdapterListLiveData();
    }

        public void loadFriendsAdapterList() {
        mMyFriendsRepository.loadMyFriendsAdapterList(getApplication());
    }

    public void getFriendlist(){
        mMyFriendsRepository.getfriendlist(getApplication());
    }
}
